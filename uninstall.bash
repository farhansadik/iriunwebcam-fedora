#!/bin/bash

echo "[*] Removing Files"
# removing files from etc 
sudo rm -rf /etc/modprobe.d/iriunwebcam-options.conf
sudo rm -rf /etcmodules-load.d/iriunwebcam.conf

# removing files from usr 
sudo rm -rv /usr/local/bin/iriunwebcam
sudo rm -rv /usr/share/applications/iriunwebcam.desktop
sudo rm -rv /usr/share/pixmaps/iriunwebcam.png

sudo dnf remove -y v4l2loopback akmod-v4l2loopback
sudo dnf autoremove 



