# Iriun Webcam for Fedora

## Installation 
1. Clone git repository: 
```bash
git clone https://gitlab.com/farhansadik/iriunwebcam-fedora.git 
```
2. Redirect to directory and run installer 
```bash
sudo ./install 
```
## Uninstall
To uninstall run the uninstaller file.  
```bash
sudo bash uninstall.bash
```

## Images 
![](images/1.png)
![](images/2.png)

### Sources <br>
<https://aur.archlinux.org/packages/iriunwebcam-bin> <br>
<https://discussion.fedoraproject.org/t/iriun-use-phone-as-a-webcam-only-deb-package/75633> <br><br>